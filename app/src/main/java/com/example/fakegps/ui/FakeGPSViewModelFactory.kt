package com.example.fakegps.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class FakeGPSViewModelFactory():ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FakeGPSViewModel() as T
    }
}