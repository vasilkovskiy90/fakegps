package com.example.fakegps.ui

import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.fakegps.domain.repository.FakeGPSRepository
import com.example.fakegps.services.GPSService
import com.example.fakegps.utils.Constants
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject

class FakeGPSViewModel : ViewModel(), KoinComponent {
    lateinit var coordinatesViewModel:CoordinatesViewModel
    private val fakeGPSRepository: FakeGPSRepository by inject()


    private val _coordsLiveData = MutableLiveData<Pair<Double, Double>>()
    val coordsLiveData: LiveData<Pair<Double, Double>>
        get() = _coordsLiveData


    fun onAddData(latitude: Double, longitude: Double) {
        coordinatesViewModel = CoordinatesViewModel(latitude, longitude)
        fakeGPSRepository.initDB(coordinatesViewModel)
    }

    fun onUpdateData(latitude: Double, longitude: Double) {
        _coordsLiveData.value = Pair(latitude, longitude)
        fakeGPSRepository.updateDBData(CoordinatesViewModel(latitude, longitude))
    }


    @RequiresApi(Build.VERSION_CODES.O)
    fun onMapClick(applicationContext: Context, coordinates: Pair<Double, Double>) {
        val intent = Intent(applicationContext, GPSService::class.java)
        GlobalScope.launch {
            intent.putExtra(Constants.COORDINATE_LATITUDE, coordinates.first)
            intent.putExtra(Constants.COORDINATE_LONGITUDE, coordinates.second)
            applicationContext.startForegroundService(intent)
        }
    }



}