package com.example.fakegps.ui

import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.fakegps.R
import com.example.fakegps.services.GPSService
import com.example.fakegps.utils.CheckPermissions
import com.example.fakegps.utils.Constants
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import org.koin.android.ext.android.inject



class MapsActivity : AppCompatActivity(), OnMapReadyCallback {
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private val checkPermissions:CheckPermissions by inject()
    private val viewModelFactory = FakeGPSViewModelFactory()
    private val viewModel: FakeGPSViewModel by lazy { ViewModelProvider(this, viewModelFactory).get(FakeGPSViewModel::class.java) }
    var defaultLocationLatitude = Constants.DEFAULT_COORDINATE
    var defaultLocationLongitude = Constants.DEFAULT_COORDINATE



    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        if (checkPermissions.onCheckPermissions(this)) {
            addLocationFunctions()
        }
    }

    private fun addMapSupport() {
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun setStartPosition() {
        viewModel.onAddData(defaultLocationLatitude, defaultLocationLongitude)
    }


    override fun onMapReady(googleMap: GoogleMap) {
        fusedLocationClient.lastLocation
                .addOnFailureListener {
                    defaultLocationLatitude = Constants.START_LOCATION_KROP_LATITUDE
                    defaultLocationLongitude = Constants.START_LOCATION_KROP_LONGITUDE
                    setStartPosition()
                    markerFirstPosition(googleMap)
                }
                .addOnSuccessListener { location: Location? ->
                    location?.latitude?.let { defaultLocationLatitude = it }
                    location?.longitude?.let { defaultLocationLongitude = it }
                    setStartPosition()
                    markerFirstPosition(googleMap)
                }

        googleMap.setOnMapClickListener {
            updateMarkerPosition(it, googleMap)
            viewModel.onUpdateData(it.latitude, it.longitude)
            //mMap.moveCamera(CameraUpdateFactory.newLatLng(it))
        }
    }

    private fun updateMarkerPosition(
            it: LatLng?,
            googleMap: GoogleMap
    ) {
        googleMap.clear()
        googleMap.addMarker(
                it?.let {marker -> MarkerOptions().position(marker) }
        )
    }

    private fun markerFirstPosition(googleMap: GoogleMap) {
        googleMap.addMarker(MarkerOptions().position(LatLng(defaultLocationLatitude, defaultLocationLongitude)))
    }

    fun onClickStop(view: View) {
        val intent = Intent(this, GPSService::class.java)
        stopService(intent)
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        when (requestCode) {
            Constants.ACCESS_COARSE_LOCATION -> {
                permissionsStatus(grantResults)
                return
            }
            Constants.ACCESS_FINE_LOCATION -> {
                permissionsStatus(grantResults)
                return
            }
            Constants.ACCESS_BACKGROUND_LOCATION -> {
                permissionsStatus(grantResults)
                return
            }
        }
    }
    private fun permissionsStatus(grantResults: IntArray){
        if (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
            addLocationFunctions()
        }
    }

    private fun addLocationFunctions() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        addMapSupport()
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onStart() {
        super.onStart()
        viewModel.coordsLiveData.observe(this,  Observer {
            if (checkPermissions.onCheckPermissions(this)) {
                viewModel.onMapClick(this, it )
            }

        })
    }
}
