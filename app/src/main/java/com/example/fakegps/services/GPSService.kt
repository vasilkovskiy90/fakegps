package com.example.fakegps.services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.IBinder
import android.os.SystemClock
import android.util.Log
import androidx.core.app.NotificationCompat
import com.example.fakegps.R
import com.example.fakegps.utils.Constants
import com.google.android.gms.location.FusedLocationProviderClient
import java.util.*
import kotlin.concurrent.scheduleAtFixedRate


class GPSService : Service() {
    private lateinit var locationProvider: FusedLocationProviderClient
    private lateinit var mockLocation: Location
    private var timer = Timer(Constants.TIMER_NAME, true)
    private lateinit var locationManager: LocationManager
    private var firstCall = false
    private val mockLocationProvider = LocationManager.GPS_PROVIDER
    private var latitude:Double? =  Constants.DEFAULT_COORDINATE
    private var longitude:Double? =  Constants.DEFAULT_COORDINATE

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (!firstCall) {
            createMockLocation(intent)
            createNotificationChannel()
            startForeground(Constants.ID, createNotification())
            firstCall = !firstCall
        } else {
            timer.cancel()
            timer = Timer(Constants.TIMER_NAME, true)
            updateLocation(intent)
        }
        return super.onStartCommand(intent, flags, startId)
    }

    private fun getCoordinates(intent: Intent?) {
        latitude = intent?.getDoubleExtra(Constants.COORDINATE_LATITUDE, Constants.DEFAULT_COORDINATE)
        longitude = intent?.getDoubleExtra(Constants.COORDINATE_LONGITUDE, Constants.DEFAULT_COORDINATE)
    }


    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(
                Constants.CHANNEL_ID,
                Constants.NOTIFICATION_NAME,
                importance
            ).apply {
                description = Constants.NOTIFICATION_CHANNEL_DESCRIPTION
                enableVibration(true)
            }

            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    private fun createNotification(): Notification {

        return NotificationCompat.Builder(applicationContext, Constants.CHANNEL_ID)
            .setStyle(NotificationCompat.BigTextStyle())
            .setSmallIcon(R.mipmap.ic_launcher)
            .setChannelId(Constants.CHANNEL_ID)
            .setContentTitle(applicationContext.getText(R.string.title_notification))
            .setContentText(
                applicationContext.getText(R.string.text_notification).toString()
            )
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setOngoing(false)
            .build()
    }


    private fun createMockLocation(intent: Intent?) {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationProvider = FusedLocationProviderClient(applicationContext)
        locationProvider.setMockMode(true)
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
            if (locationManager.isProviderEnabled(mockLocationProvider)) {
                locationManager.removeTestProvider(mockLocationProvider)
            }
        }
        locationManager.addTestProvider(
            mockLocationProvider, false, false, false, false, false, true, false,
            Constants.POWER_REQUIREMENT, Constants.LOCATION_ACCURACY.toInt()
        )
        locationManager.setTestProviderEnabled(mockLocationProvider, true)
        mockLocation = Location(mockLocationProvider)
        updateLocation(intent)

    }

    override fun onDestroy() {
        super.onDestroy()
        timer.cancel()
    }

    private fun updateLocation(intent: Intent?) {
        timer.scheduleAtFixedRate(
            Constants.TIMER_DELAY,
            Constants.TIMER_PERIOD
        ) {
            getCoordinates(intent)
            latitude?.let { mockLocation.latitude = it }
            longitude?.let { mockLocation.longitude =it }
            mockLocation.time = System.currentTimeMillis()
            mockLocation.accuracy = Constants.LOCATION_ACCURACY
            mockLocation.speed = Constants.LOCATION_SPEED
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                mockLocation.elapsedRealtimeNanos = SystemClock.elapsedRealtimeNanos()
            }
            locationManager.setTestProviderLocation(mockLocationProvider, mockLocation)
            locationProvider.setMockLocation(mockLocation)
        }
    }
}
