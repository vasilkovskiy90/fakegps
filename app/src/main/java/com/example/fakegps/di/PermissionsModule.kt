package com.example.fakegps.di

import com.example.fakegps.utils.CheckPermissions
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

val permissionsModule: Module = module {
    single { CheckPermissions() }
}