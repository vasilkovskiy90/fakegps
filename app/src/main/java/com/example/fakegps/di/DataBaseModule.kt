package com.example.fakegps.di

import com.example.fakegps.domain.database.DataBase
import org.koin.android.ext.koin.androidContext

import org.koin.core.module.Module
import org.koin.dsl.module

val dataBaseModule: Module = module {
    single { DataBase(androidContext()) }
}