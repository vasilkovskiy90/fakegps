package com.example.fakegps.di

import com.example.fakegps.domain.repository.FakeGPSRepository
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

val repositoryModule: Module = module {
    single { FakeGPSRepository(androidContext()) }
}