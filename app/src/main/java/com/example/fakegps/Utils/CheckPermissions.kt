package com.example.fakegps.utils

import android.Manifest
import android.app.AppOpsManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Process
import android.provider.Settings
import android.provider.Settings.ACTION_DEVICE_INFO_SETTINGS
import android.provider.Settings.Secure
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.fakegps.BuildConfig
import com.example.fakegps.R
import com.example.fakegps.ui.MapsActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_maps.*
import org.koin.core.KoinComponent


class CheckPermissions : KoinComponent {
    lateinit var context: Context

    @RequiresApi(Build.VERSION_CODES.Q)
    fun onCheckPermissions(mapsActivity: MapsActivity): Boolean {
        this.context = mapsActivity.applicationContext
        if (!onCheck(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Constants.ACCESS_COARSE_LOCATION,
                mapsActivity
            ) &&
            !onCheck(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Constants.ACCESS_FINE_LOCATION,
                mapsActivity
            )
        ) {
            return false
        }
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.Q &&
            !onCheck(
                Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                Constants.ACCESS_BACKGROUND_LOCATION,
                mapsActivity
            )
        ) {
            return false
        }
        if (Secure.getInt(
                mapsActivity.contentResolver,
                Settings.Global.DEVELOPMENT_SETTINGS_ENABLED,
                0
            ) == 0
        ) {
            addSnackBar(mapsActivity, ACTION_DEVICE_INFO_SETTINGS, R.string.maps_activity_developer_options_message)

            return false
        }
        if (Secure.getInt(
                mapsActivity.contentResolver,
                Secure.LOCATION_MODE,
                Secure.LOCATION_MODE_OFF
            ) == 0
        ) {
            addSnackBar(
                mapsActivity,
                Settings.ACTION_LOCATION_SOURCE_SETTINGS,
                R.string.maps_activity_location_options_message
            )
            return false
        }
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M &&
            Secure.getInt(mapsActivity.contentResolver, Secure.ALLOW_MOCK_LOCATION) == 0
        ) {
            Snackbar.make(
                mapsActivity.stopButton,
                context.getText(R.string.maps_activity_mock_location_message),
                Snackbar.LENGTH_LONG
            )
                .show()
            return false
        }
        val opsManager = context.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M &&
            opsManager.checkOpNoThrow(
                AppOpsManager.OPSTR_MOCK_LOCATION, Process.myUid(), BuildConfig.APPLICATION_ID
            ) != AppOpsManager.MODE_ALLOWED
        ) {
            addSnackBar(
                mapsActivity,
                Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS,
                R.string.maps_activity_mock_location_message
            )
            return false
        }


        return true
    }

    private fun addSnackBar(
        mapsActivity: MapsActivity,
        settings: String,
        message: Int
    ) {
        val mockLocationSnackbar = Snackbar.make(
            mapsActivity.stopButton,
            context.getText(message),
            Snackbar.LENGTH_INDEFINITE
        )
        mockLocationSnackbar.setAction(
            R.string.mock_location_snackbar_go_to,
            SnackActionListener(settings)
        )
        mockLocationSnackbar.show()
    }


    private fun onCheck(
        permission: String,
        requestCode: Int,
        mapsActivity: MapsActivity
    ): Boolean {
        val permissionStatus = ContextCompat.checkSelfPermission(
            context,
            permission
        )
        if (permissionStatus == PackageManager.PERMISSION_GRANTED) {
            return true
        }
        ActivityCompat.requestPermissions(
            mapsActivity,
            arrayOf(permission),
            requestCode
        )
        return false
    }


}