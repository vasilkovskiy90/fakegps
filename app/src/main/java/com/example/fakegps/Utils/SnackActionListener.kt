package com.example.fakegps.utils

import android.content.Intent
import android.view.View

class SnackActionListener(private val settings:String) : View.OnClickListener{
        override fun onClick(v: View) {
           v.context.startActivity(
               Intent(settings).addFlags(
                   Intent.FLAG_ACTIVITY_NEW_TASK
               ))
        }
}