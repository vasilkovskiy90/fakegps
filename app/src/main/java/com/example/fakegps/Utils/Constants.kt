package com.example.fakegps.utils

object Constants {

    const val ID = 1
    const val CHANNEL_ID = "GPSService Notification"
    const val NOTIFICATION_NAME = "Personal Notification"
    const val NOTIFICATION_CHANNEL_DESCRIPTION = "Include all the personal notifications"
    const val START_LOCATION_KROP_LATITUDE = 48.5064266
    const val START_LOCATION_KROP_LONGITUDE = 32.2597293
    const val DATABASE_NAME = "fakeGPS-database"
    const val COORDINATE_LATITUDE = "latitude"
    const val COORDINATE_LONGITUDE = "longitude"
    const val TIMER_NAME = "GPSTimer"
    const val TIMER_PERIOD: Long = 500
    const val LOCATION_ACCURACY = 5f
    const val LOCATION_SPEED = 10f
    const val POWER_REQUIREMENT = 1
    const val TIMER_DELAY: Long = 0
    const val DEFAULT_COORDINATE: Double = 0.0
    const val ACCESS_COARSE_LOCATION = 1
    const val ACCESS_FINE_LOCATION = 2
    const val ACCESS_BACKGROUND_LOCATION = 3


}