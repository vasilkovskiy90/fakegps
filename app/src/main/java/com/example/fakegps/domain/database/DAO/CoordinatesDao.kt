package com.example.fakegps.domain.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.fakegps.repository.entities.CoordinatesEntity


@Dao
interface CoordinatesDao {

    @Insert
    fun insert(coordinates: CoordinatesEntity?)

    @Query("SELECT * FROM coordinatesentity")
    fun getCoordinates(): List<CoordinatesEntity?>?

    @Delete
    fun delete(coordinates: CoordinatesEntity?)
}