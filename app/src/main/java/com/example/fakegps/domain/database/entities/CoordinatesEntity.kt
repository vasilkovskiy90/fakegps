package com.example.fakegps.repository.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CoordinatesEntity(@PrimaryKey var latitude: Double, var longitude: Double) {

    override fun toString(): String {
        return "$latitude : $longitude"
    }

}