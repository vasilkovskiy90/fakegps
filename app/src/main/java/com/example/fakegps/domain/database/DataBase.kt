package com.example.fakegps.domain.database

import android.content.Context
import androidx.room.Room
import com.example.fakegps.domain.Mapper
import com.example.fakegps.domain.database.dao.AppDatabase
import com.example.fakegps.domain.repository.CoordinatesRepository
import com.example.fakegps.repository.entities.CoordinatesEntity

import com.example.fakegps.utils.Constants
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DataBase(val androidContext: Context) {
    var db: AppDatabase? = null


    fun init(coordinates: CoordinatesRepository) {
        val data = DataBaseMapper().map(coordinates)
        db = Room.databaseBuilder(
            androidContext,
            AppDatabase::
            class.java, Constants.DATABASE_NAME
        ).build()
        addFirstData(data.latitude, data.longitude)
    }


    private fun addFirstData(latitude: Double, longitude: Double) {
        GlobalScope.launch {
            if (db?.coordinatesDao!!.getCoordinates()?.isNotEmpty()!!) {
                val oldCoordinates = db?.coordinatesDao?.getCoordinates()?.first()
                db?.coordinatesDao?.delete(oldCoordinates)
            }
            db?.coordinatesDao?.insert(
                CoordinatesEntity(
                    latitude,
                    longitude
                )
            )
        }
    }

    fun updateData(coordinatesRepository: CoordinatesRepository) {
        val dataToUpdate = DataBaseMapper().map(coordinatesRepository)
        GlobalScope.launch {
            val oldCoordinates = db?.coordinatesDao?.getCoordinates()?.first()
            db?.coordinatesDao?.delete(oldCoordinates)
            db?.coordinatesDao?.insert(dataToUpdate)

        }
    }

    fun getDBData(): CoordinatesEntity? {
        val latitude = db?.coordinatesDao?.getCoordinates()?.first()?.latitude
        val longitude = db?.coordinatesDao?.getCoordinates()?.first()?.longitude

        if (latitude != null && longitude != null) {
            return CoordinatesEntity(latitude, longitude)
        }
        return null
    }

    class DataBaseMapper: Mapper<CoordinatesRepository, CoordinatesEntity> {
        override fun map(input: CoordinatesRepository): CoordinatesEntity {
            return CoordinatesEntity(latitude = input.latitude, longitude = input.longitude)
        }
    }


}