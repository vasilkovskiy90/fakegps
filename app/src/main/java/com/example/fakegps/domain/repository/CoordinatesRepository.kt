package com.example.fakegps.domain.repository


data class CoordinatesRepository(var latitude: Double, var longitude: Double) {
}