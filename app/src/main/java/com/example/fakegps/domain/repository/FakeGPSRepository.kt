package com.example.fakegps.domain.repository

import android.content.Context
import com.example.fakegps.domain.Mapper
import com.example.fakegps.domain.database.DataBase
import com.example.fakegps.repository.entities.CoordinatesEntity
import com.example.fakegps.ui.CoordinatesViewModel
import org.koin.core.KoinComponent
import org.koin.core.inject


class FakeGPSRepository(private val context: Context):KoinComponent {

    private val dataBase: DataBase by inject()

    fun initDB(coordinatesViewModel:CoordinatesViewModel) {
        val coordinatesRepository = RepositoryMapper()
            .map(coordinatesViewModel)
        dataBase.init(coordinatesRepository)
    }

    fun updateDBData(coordinatesViewModel:CoordinatesViewModel){
        val coordinatesRepository = RepositoryMapper()
            .map(coordinatesViewModel)
        dataBase.updateData(coordinatesRepository)
    }

    fun getData(): CoordinatesRepository?{
        return dataBase.getDBData()?.let {
            RepositoryFromDBMapper()
                .map(it)
        }
    }


    class RepositoryMapper:Mapper<CoordinatesViewModel, CoordinatesRepository>{
        override fun map(input: CoordinatesViewModel): CoordinatesRepository {
            return CoordinatesRepository(latitude = input.latitude, longitude = input.longitude)
        }
    }
    class RepositoryFromDBMapper:Mapper<CoordinatesEntity, CoordinatesRepository>{
        override fun map(input: CoordinatesEntity): CoordinatesRepository {
            return CoordinatesRepository(latitude = input.latitude, longitude = input.longitude)
        }
    }

}