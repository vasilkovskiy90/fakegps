package com.example.fakegps.domain

interface Mapper<I, O> {
fun map (input: I):O
}