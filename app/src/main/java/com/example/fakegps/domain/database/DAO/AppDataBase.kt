package com.example.fakegps.domain.database.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.fakegps.repository.entities.CoordinatesEntity


@Database(
    entities = [CoordinatesEntity::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract val coordinatesDao: CoordinatesDao?
}